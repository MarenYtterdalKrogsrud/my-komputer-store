
//an array with the mock data for the laptops
const LAPTOPS = [
    {
        id: 1,
        name: "HP ProBook 430",
        price: 500,
        description:"Reliable HP ProBook 430 has full functionality, it is thin and light, and offers key commercial features at an affordable price for any business.",
        features: ["Ram: 8 GB","Speed: 2400 MHz","Screen: 13.3"],
        image: 'img/hpPro.webp'
    },
    {
        id: 2,
        name: "Acer Chromebook 315 ",
        price: 600,
        description:"Designed for life on the go, the Chromebook has an anti-reflective coating and a large 15-inch screen so you can do more. It is equipped with its own numeric keypad, fast CPU and long battery life - it gives you longer work sessions and it is more powerful than before.",
        features: ["Ram: 4","Speed: 2.8 GHz","Screen: 15.6"],
        image: 'img/Acer.webp'
    },
    {
        id: 3,
        name: "Lenovo IdeaPad 5",
        price: 450,
        description:"With IdeaPad 5, you get what you need for performance, connectivity and on-the-go entertainment, with fast AMD Ryzen 4000 processors. The images on the 15.6 \"FHD screen are sharp and are in the style of the sound from the forward-facing speakers with Dolby Audio.",
        features: ["Ram: 8 GB","Speed: 4 GHz","Screen: 15.6"],
        image: 'img/lenovo.jpg'
    },
    {
        id: 4,
        name: "Huawei MateBook D",
        price: 500,
        description:"Disappear into a 15.6 beautiful IPS FullView screen. With an incredible 87% screen ratio and a thin frame that has been reduced to just 5.3 mm, it is perfect for watching movies, being creative or getting rid of something work.",
        features: ["Ram: 8 GB","Speed: 3.7 GHz","Screen: 15.6"],
        image: 'img/huawei.webp'
    }
]

//variables for the elements by their id
const amountEl = document.getElementById("amount");
const loanEl = document.getElementById("outstandingLoan");
const payEl = document.getElementById("pay");
const selectEl = document.getElementById("computers");
const loanButton = document.getElementById("loanButton");
const buyButton = document.getElementById("buyButton");

//set the variables for bank balance, the pay, the loan
let balance = 200;
let payAmount = 0;
let loanAmount = 0;

//variable for the chosen laptop
let selectedLaptop;

//variables to count number of times the user have taken and bought a computer
let countLoan = 0;
let countBuyComputer = 0;

amountEl.innerText = balance;
payEl.innerText = payAmount;

// loops through the laptop array to generate select options.
for(const laptop of LAPTOPS) {
    const el = document.createElement("option");
    el.value = laptop.id;
    el.innerText = laptop.name;
    selectEl.appendChild(el);
}

// eventlistener for the select change. Display the chosen laptop from the dropdown
selectEl.addEventListener('change', function (){
    const value = Number(this.value);
    const myNode = document.getElementById('image');
    myNode.innerHTML =""; //removes the image when a new laptop is chosen
    selectedLaptop = LAPTOPS.find((x) => x.id === value);

    // gets the elements by id and add text
    document.getElementById('computerName').innerText = selectedLaptop.name;
    document.getElementById('price').innerText = "Price: " + selectedLaptop.price + " kr";
    document.getElementById('description').innerText = selectedLaptop.description;
    document.getElementById('features').innerText = selectedLaptop.features;
    const image = new Image(); //create an object to display the image
    image.src = selectedLaptop.image; //get the source to the images
    document.getElementById('image').appendChild(image); // display image
    const chooseEl = document.getElementById('choose'); //ass a choose option to dropdown

    // remove "choose" when the user have chosen an element
    if (chooseEl) {
        chooseEl.remove();
    }
})
// eventlistener function for click events
addEventListener('click', function (){
    if (loanAmount > 0) {   // check if the user has a loan to display button or not
        loanButton.style.display = "initial";
    } else {
        loanButton.style.display = "none";
        loanEl.innerText = ""; // to not display loan text if there is no loan
    }
    // check if a laptop is selected to  hide or show the buy button
    if (selectedLaptop){
        buyButton.style.display = "initial"
    } else {
        buyButton.style.display= "none";
    }
})
// this function is to get a loan
function getLoan(){
    if (countLoan > countBuyComputer){ //check if the user have bought a laptop before getting a new loan
        alert("You have to buy a laptop before you can get a new loan");
        return;
    }
    if(loanAmount === 0){ // To check that the user only get one loan before paying back
        let amount = prompt("Enter the amount you want to loan: ", "amount here");
        let amountDouble = balance*2;
        let boolean = true;
        while(boolean) { //loop to get the user to type in the correct number.
            if (Number(amount) <= amountDouble) { //
                balance += Number(amount); //to check the valid amount to loan
                loanAmount += Number(amount); //add the amount to the loan amount
                loanEl.innerText = "Outstanding loan: " + loanAmount + " kr"; //display text
                amountEl.innerText = balance; //adds the loan to the balance
                boolean = false;
                countLoan ++; // count number of times a user has got a loan
            } else {
                amount = prompt("This amount is to high. Try to type in a smaller number!");

            }
        }

    }else{
        alert("You can't get a new loan before you pay it back!")
    }
}

// function to calculate the payment
function getPaid(){
    payAmount += 100; // adds 100 every payment
    if(loanAmount>0){  // check if there is a loan as it should pay 10% to the loan
        let payLoan = (10 / 100) * payAmount; // calculate 10%
        loanAmount -= payLoan;
    }

    // display value
    payEl.innerText = payAmount;
    loanEl.innerText = "Outstanding loan: " + loanAmount + " kr";
}

// transfer the payment to the bank account
function transferPaymentBalance(){
  let paid = payAmount;
  balance += Number(paid); // add the payment to the balance
    payAmount = 0; // set to 0
    amountEl.innerText = balance;
    payEl.innerText = payAmount;
}

// transfer the payment to the loan
function transferPaymentLoan(){
    let paid = payAmount;
    if(paid > loanAmount) {  // check if the payment is greater then the loan to add the diff to the balance
        const diff = paid - loanAmount; // calculate the diff
        loanAmount = 0;
        balance += diff; // add the diff to the account balance
        amountEl.innerText = balance;
    } else {
        loanAmount -= paid;
    }
    payAmount = 0;
    payEl.innerText = payAmount;
    loanEl.innerText = "Outstanding loan: " + loanAmount + " kr";
}

// buy the laptop
function buyComputer(){
    let price = selectedLaptop.price; // get the laptop price from the selected laptop
    if(price > balance){  // check if the user has enough money
        alert("You dont have enough money!")
        return;
    }
   balance -= price; // calculate the diff
   countBuyComputer++;
    alert("You have just bought yourself a new laptop. Enjoy!")
    amountEl.innerText = balance;
}




